import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

/**
 * TODO A DAGMap :)
 * @author Callan Gray, James Udiljak
 *
 */
public class DAGMap<K,V> {
	private Map<K,Set<K>> graph;
	private Map<K,V> map;
	
	/**
	 *TODO Non specified constructor
	 */
	public DAGMap() {
		//TODO Maybe rewrite for linkedList here
		graph = new HashMap<K,Set<K>>();
		map = new HashMap<K,V>();
	}
	
	/**
	 * 
	 * @param dag
	 */
	public DAGMap(DAGMap<K,V> dagmap) {
		graph = new HashMap<K,Set<K>>((int)(dagmap.graph.size()/0.75)+1);
		for(K key : dagmap.graph.keySet()) graph.put(key, new HashSet<K>(dagmap.graph.get(key)));
		map = new HashMap<K,V>(map);
	}
	
	/**
	 * Standard map operation: Adds the vertex key to the graph 
	 * (with no edges) and maps key to value.
	 * @param key the new vertex added to the graph.
	 * @param value the label of the vertex.
	 * @throws IllegalArgumentException if key is null, or is 
	 * already in the graph.
	 */
	public void put(K key, V value) throws IllegalArgumentException {
		//Constant time, hashmap put methods are constant.
		if(key == null) throw new IllegalArgumentException("key is null");
		if(map.containsKey(key)) throw new IllegalArgumentException("key is already inside DAGMap");
		//HashSet used because it is faster than all set iterators funny enough
		//http://www.programcreek.com/2013/03/hashset-vs-treeset-vs-linkedhashset/
		graph.put(key, new HashSet<K>());
		map.put(key, value);
	}
	
	/**
	 * Standard map operation: Gives the value corresponding to the given key 
	 * @param key the vertex to return the value
	 * @throws IllegalArgumentException if the key does not exist in the DAG.
	 */
	public V get(K key) throws IllegalArgumentException {
		//Constant time, hashmap get methods are constant
		if(!map.containsKey(key)) throw new IllegalArgumentException("key does not exist in map");
		return map.get(key);
	}
	
	/**
	 * Standard map operation: Removes the vertex key from the graph, as well as its label, and any associated edges.
	 * @param key the vertex to be removed.
	 * @throws IllegalArgumentException if key is not a vertex in the graph.
	 */
	public void remove(K key) throws IllegalArgumentException {
		// TODO worse case O(E)
		if(!graph.containsKey(key)) throw new IllegalArgumentException("key does not exist in graph");
		map.remove(key);
		graph.remove(key);
		for(Set<K> vertex : graph.values()) {
			vertex.remove(key);
		}
	}
	
	/**
	 * DAG operation: adds a directed edge between two vertices, provided that no cycle is created.
	 * @param requirement the vertex at the start of the edge.
	 * @param dependant the vertex at the end of the edge.
	 * @throws IllegalArgumentException if either requirement or dependent are not elements of the graph,
	 *  or if the new edge would create a cycle.
	 */
	public void addDependency(K requirement, K dependent) throws IllegalArgumentException {
		
		if(!graph.containsKey(requirement)) throw new IllegalArgumentException("Requirement is not in the graph.");
		if(!graph.containsKey(dependent)) throw new IllegalArgumentException("Dependent is not in the graph.");
		if(isDependant(dependent, requirement)) throw new IllegalArgumentException("Dependency will create a cycle.");
		if(requirement.equals(dependent)) throw new IllegalArgumentException("Key cannot be dependent of itself.");
		// TODO Not sure whether to throw error if edge already exists!
		// Makes sense to use a set for storing dependencies as each dependency should be unique.
		graph.get(requirement).add(dependent);
	}
	
	/**
	 * DAG operation: removes an edge between two vertices, if it exits.
	 * @param requirement the vertex at the start of the edge
	 * @param dependancy the vertex at the end of the edge.
	 * @throws IllegalArgumentException if either requirement or dependent are not vertices in the graph.
	 */
	public void removeDependancy(K requirement, K dependent) throws IllegalArgumentException {
		if(!graph.containsKey(requirement)) throw new IllegalArgumentException("Requirement is not in the graph.");
		if(!graph.containsKey(dependent)) throw new IllegalArgumentException("Dependancy is not in the graph.");
		graph.get(requirement).remove(dependent);
	}
	
	/**
	 * Tests whether there are any vertices defined in the graph.
	 * @return true if and only if there are no vertices in the graph.
	 */
	public boolean isEmpty() {
		return graph.isEmpty();
	}
	
	/**
	 * Tests whether a key is defined in the map, using the equals method.
	 * @param key the key to be tested.
	 * @return true if and only if the key is a vertex of the graph.
	 */
	public boolean containsKey(K key) {
		return map.containsKey(key);
	}
	
	/**
	 * Tests whether there is some vertex in the graph that has a label equal to value.
	 * @param value the value to be tested.
	 * @return true if and only if there is at least one vertex in the graph with a label equal to value.
	 */
	public boolean containsValue(V value) {
		return map.containsValue(value);
	}
	
	/**
	 * Tests whether key1 is a requirement for key2 (or is key2 is dependent on key1) This is defined 
	 * by either there being an edge from key1 to key2, or there being some other vertex v that is 
	 * dependent on key1, where there is an edge from v to key2.
	 * @param key1 the requirement vertex.
	 * @param key2 the dependent vertex.
	 * @return true if and only if key1 is a requirement for key2. If either key is not an element of 
	 * the graph, return false.
	 */
	public boolean isDependant(K key1, K key2) {
		//Depth first search to key, worst case O(E)
		Set<K> vertex1 = graph.get(key1);
		if(vertex1.contains(key2)) return true;
		for(K key : vertex1) if (isDependant(key, key2)) return true;
		return false;
	}
	
	/**
	 * Creates a shallow clone of the DAGMap, by cloning the keys (vertices) but not the values.
	 * @return Object A DAGMap that is equal to this map, but with cloned vertices.
	 */
	public Object clone() {
		//O(V) * however long it takes to clone a hashset + however long it takes to clone a hashmap
		DAGMap<K,V> cloneDAGMap = new DAGMap<K,V>();
		for(K key : graph.keySet()) cloneDAGMap.graph.put(key, new HashSet<K>(graph.get(key)));
		cloneDAGMap.map = new HashMap<K,V>(map);
		return cloneDAGMap;
	}

	/**
	 * Produces a iterator that is guaranteed to process the elements in some topologically sorted
	 * order where each call to next will return the next vertex That is, the iterator will never return
	 * a dependent vertex until all of the requirements of the vertex have been returned.
	 * @return an iterator that will process the labels of the graph in topologically sorted order.
	 */
	public Iterator<K> iterator() {
		//Worse case O(E) + get + contains + add
		LinkedList<K> topologicalList = new LinkedList<K>();
		for(K root : getIndependents()) topologicalSort(root, 0, topologicalList);
		return topologicalList.iterator();
	}
	
	private void topologicalSort(K parent, Integer index, LinkedList<K> tplist) {
		//O(E)
		tplist.add(index++, parent);
		for(K child : graph.get(parent)) if(!tplist.contains(child)) topologicalSort(child, index, tplist);
	}
	
	/**
	 * Tests whether this is equal to some object: The Object must be an implementation of DAGMap and the
	 * two DAGMaps are equal if they have :
	 * equal sets of keys,
	 * equal sets of values,
	 * equal keys map to equal values, 
	 * there is a dependency (not just an edge) between two keys in one graph if and only there is a dependency 
	 * between the two equal keys in the second graph.
	 * @param o object to be compared to.
	 * @return true if both objects are equal.
	 */
	@Override
	public boolean equals(Object o) {
		// TODO Need to test with the clone method.
		if(o == this) return true;
		if (o == null) return false;
		if (!(o instanceof DAGMap<?,?>)) return false;
		DAGMap<?,?> other = (DAGMap<?,?>)o;
		if (!map.equals(other.map)) return false;
		if (!graph.equals(other.graph)) return false;
		return true;
	}
	
	/**
	 * Returns the set of vertices in the DAG.
	 * @return the set of vertices in the graph.
	 */
	public Set<K> getKeySet() {
		return new HashSet<K>(graph.keySet());
	}
	
	/**
	 * Returns the set of keys that are dependent on the given key 
	 * (that is, there is a path from key to exactly the vertices in the returned set).
	 * @param key the vertex on which the returned set must be dependent.
	 * @return the set of vertices dependent on key.
	 * @throws IllegalArgumentException if key is not a vertex of the DAG.
	 */
	public Set<K> getDependents(K key) throws IllegalArgumentException {
		if(!graph.containsKey(key)) throw new IllegalArgumentException("key is not in graph.");
		Set<K> set = new HashSet<K>((int)(graph.size()/0.75)+1);
		Queue<K> queue = new LinkedList<K>();
		queue.add(key);
		while(!queue.isEmpty()) {
			K x = queue.poll();
			for(K y : graph.get(x)) if(!set.contains(y)) {
				queue.add(y);
				set.add(y);
			}
		}
		return set;
	}
	
	/**
	 * Returns the set of keys that are requirements for the given key
	 * (that is, there is a path from every vertex, and only these vertices, in the returned set to key).
	 * @param key the vertex on which the returned set must be requirements.
	 * @return the set of vertices required for key.
	 * @throws IllegalArgumentException if key is not a vertex of the DAG.
	 */
	public Set<K> getRequirements(K key) throws IllegalArgumentException {
		Set<K> set = new HashSet<K>((int)(graph.size()/0.75)+1);
		Stack<K> currentPath;
		Set<Set<K>> paths = new HashSet<Set<K>>((int)(graph.size()/0.75)+1);
		for(K source : getIndependents()) {
			currentPath = new Stack<K>();
			requirementsDFS(source, key, currentPath, paths);
		}
		for( Set<K> p : paths) set.addAll(p);
		return set;
	}
	
	/**
	 * A recursive method that performs a depth first search of the graph that determines
	 * all the possible paths from the source to the sink.
	 * @param source
	 * @param sink
	 * @param currentPath
	 * @param paths
	 */
	private void requirementsDFS(K source, K sink, Stack<K> currentPath, Set<Set<K>> paths) {
		currentPath.add(source);
		for(K y : graph.get(source)) {
			if(y.equals(sink)) paths.add(new HashSet<K>(currentPath));
			else requirementsDFS(y, sink, currentPath, paths);
		}
		currentPath.remove(source);
	}
	
	/**
	 * The width of a DAG is the minimum number of paths required to cover it. A path is a sequence 
	 * (v(1),v(2),..,v(n)) where for each i greater than 1, v(i) is dependent on v(i-1): 
	 * This method should aim to return the minimum number of sequences such that every vertex appears 
	 * in at least one sequence (but potentially more): for example the width of an empty DAGMap is 0, 
	 * and if there are n vertices with no requirements, then the width will be at least n.
	 * @return the width of the DAGMap.
	 */
	public int getWidth() {
		List<List<K>> paths = new LinkedList<List<K>>();
		Stack<K> currentPath = new Stack<K>();
		for (K indep : getIndependents()) widthDFS(indep, currentPath, paths);
		
		Iterator<List<K>> it = paths.iterator();
		while(it.hasNext()) {
			List<K> path = it.next();
			Set<K> otherVertices = new HashSet<K>((int)(graph.size()/0.75)+1);
			for(List<K> otherPath : paths) if(otherPath != path) otherVertices.addAll(otherPath);
			if(otherVertices.containsAll(path)) it.remove();
		}
		return paths.size();
	}
	
	/**
	 * A recursive function that performs a depth first search of the graph from
	 * the given key to nodes that have no dependents. Visited vertices in the search 
	 * are stored in currentPath. When the search reaches a vertex that has no dependents, 
	 * the current path is added to paths, otherwise continues searching.
	 * @param key node to continue searching from.
	 * @param currentPath a collection of nodes that represents the current path
	 * that has been found.
	 * @param paths a collection of complete paths that currentPath has found.
	 */
	private void widthDFS(K key, Stack<K> currentPath, List<List<K>> paths) {
		currentPath.push(key);
		if(graph.get(key).isEmpty()) paths.add(new ArrayList<K>(currentPath));
		else for(K next : graph.get(key)) widthDFS(next, currentPath, paths);
		currentPath.pop();
	}
	
	/**
	 * A function that iterates through all the edges in graph to determine
	 * all the vertices that have no requirements.
	 * @return a set of all the independent vertices in the graph.
	 */
	private Set<K> getIndependents() {
		//Start with all the keys then remove the destination vertex of every edge.
		Set<K> independentSet = getKeySet();
		for(Set<K> vertex : graph.values()) for(K key : vertex) independentSet.remove(key);
		return independentSet;
	}
	
	/**
	 * Returns the length of the longest possible path through the DAGMap A path is a sequence 
	 * (v(1),v(2),..,v(n)) where for each i greater than 1, v(i) is dependent on v(i-1).
	 * @return the length of the longest possible path in the DAGMap.
	 */
	public int getLongestPath() {
		Map<K,Integer> distances;
		int longest = 0;
		//Longest path will always start from an independent vertex.
		for (K ind : getIndependents()) {
			distances = new HashMap<K,Integer>();
			Queue<K> queue = new LinkedList<K>();
			queue.offer(ind);
			distances.put(ind, 0);
			while(!queue.isEmpty()) {
				K x = queue.poll();
				for (K y : graph.get(x)) {
					int dist = distances.get(x) + 1;
					if(distances.get(y) == null || dist > distances.get(y)) {
						distances.put(y, dist);
						queue.offer(y);
					}
					if(dist > longest) longest = dist;
				}
				
			}
		}
		return longest;
	}
	
	/**
	 * Gets the maximum flow between two vertices in the DAG; The maximum flow is the maximum number 
	 * of paths through the DAG that do not share a vertex. 
	 * @param source the start vertex.
	 * @param sink the end vertex.
	 * @return The maximum flow through the DAG.
	 */
	public int getMaxFlow(K source, K sink) {
		//Ford Fulkerson method
		int flow = 0;
		Map<K,Set<K>> flowEdges = new HashMap<K,Set<K>>(graph);
		Map<K,Set<K>> backFlowEdges = new HashMap<K,Set<K>>((int)(graph.size()/0.75)+1);
		for(K key : graph.keySet()) backFlowEdges.put(key, new HashSet<K>((int)(graph.size()/0.75)+1));
		Set<K> reversedVertices = new HashSet<K>((int)(graph.size()/0.75)+1);
		Stack<K> currentPath = new Stack<K>();
		while(flowDFS(false, source, sink, currentPath, flowEdges, backFlowEdges, reversedVertices)) {
			boolean usesInput = false;
			while(currentPath.size() > 1) {
				K y = currentPath.pop();
				K x = currentPath.peek();
				//must be a forward edge if it is in the graph, otherwise is a back edge
				if(graph.get(x).contains(y)) {
					flowEdges.get(x).remove(y);
					backFlowEdges.get(y).add(x);
					if(usesInput) reversedVertices.add(y);
					usesInput = true;
				}
				else {
					backFlowEdges.get(x).remove(y);
					flowEdges.get(y).add(x);
					if(!usesInput) reversedVertices.remove(y);
					usesInput = false;
				}
			}
			currentPath.clear();
			flow++;
		}
		return flow;
	}
	
	/**
	 * A recursive depth first search algorithm to find a path from source to sink with unique vertices in a
	 * graph consisting of both forwards residual edges and backwards flow edges.
	 * @param entry true if the path to vertex was a forwards residual edge.
	 * @param source the vertex to search from.
	 * @param sink the vertex to search for.
	 * @param currentPath reference of the vertices currently used in the search path.
	 * @param flowEdges a graph of edges representing possible paths to flow through.
	 * @param backFlowEdges a graph of edges that represents previously used edges that can have backwards flow.
	 * @param usedVertices a set of vertices that already have flow passing through them.
	 * @return true if a path to the sink was found.
	 */
	private boolean flowDFS(boolean entry, K source, K sink, Stack<K> currentPath, Map<K,Set<K>> flowEdges, Map<K,Set<K>> backFlowEdges, Set<K> usedVertices) {
		if(currentPath.contains(source)) return false; //Check for loops
		currentPath.push(source);
		if(source.equals(sink)) return true;
		if(!usedVertices.contains(source) || (!entry)) for(K x : flowEdges.get(source)) {
			if(flowDFS(true, x, sink, currentPath, flowEdges, backFlowEdges, usedVertices)) return true;
		}
		if(entry) for(K x : backFlowEdges.get(source)) {
			if(flowDFS(false, x, sink, currentPath, flowEdges, backFlowEdges, usedVertices)) return true;
		}
		currentPath.pop();
		return false;
	}
	
	/**
	 * Returns a multi-line String representation of the DAGMap The representation should 
	 * fist present the mapping in the form
	 * key : value
	 * one per line, where the order of the pairs is not important. Then, the representation 
	 * should give the set of dependencies to completely describe the graph key1 : key2.
	 * @return a String representation of the DAGMap.
	 */
	@Override
	public String toString() {
		StringBuilder strb = new StringBuilder();
		for(K key : map.keySet()) {
			strb.append(key.toString());
			strb.append(" : ");
			if(map.get(key) != null) strb.append(map.get(key).toString());
			else strb.append("null");
			strb.append("\n");
		}
		strb.append("\n");
		for(K key1 : graph.keySet()) {
			for(K key2 : graph.get(key1)) {
				strb.append(key1.toString());
				strb.append(" : ");
				strb.append(key2.toString());
				strb.append("\n");
			}
		}
		return strb.toString();
	}
}