import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;


public class DAGMapTester {
	
	
	
	DAGMap<Character,String> dagm;
	DAGMap<Character,String> hard;
	DAGMap<Character,String> harder;
	final char[] keys = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u'};
	final String[] values = {"This", "is", "a", "very", "complicated", "way", "of", "storing", "a", "simple", "sentence", null};
	
	/*
	 * Setup will create a DAGMap of this graph
	 * dagm
	 * a <- d    g <- j
	 * |    ^    ^    ^
	 * v    |    |    | 
	 * b    e -> h -> k
	 * |    ^    ^    
	 * v    |    |    
	 * c <- f -> i <- l
	 * 
	 * that links to this map
	 * a:This
	 * b:is
	 * c:a
	 * d:very
	 * e:complicated
	 * f:way
	 * g:of
	 * h:storing
	 * i:a
	 * j:simple
	 * k:sentence
	 * l:null
	 * 
	 * A better dag to test max flow.
	 * Only one possible way of getting flow of 3.
	 * 
	 * hard
	 * a => d => g =>[j]<= m
	 * ^    ^    |    ^    ^
	 * U    |    v    U    U
	 * b -> e => h => k -> n
	 * ^    ^    |    ^    ^
	 * U    U    v    |    U
	 * c <=[f]-> i => l => o
	 * 
	 * An even better one that will trump bfs when the shortest
	 * path is not a max flow path
	 * 
	 * harder
	 * a->d->f->h->j---------->s
	 * ^           ^           |
	 * |           |           v
	 * b->e->g->i->k->m->o->q->t
	 * |           ^           ^
	 * v           |           |
	 * c---------->l->n->p->r->u
	 * 
	 */
	@Before
	public void setUp() {
		dagm = new DAGMap<Character,String>();
		
		for(int i = 0; i < 12; i++) {
			dagm.put(keys[i], values[i]);
		}
		
		dagm.addDependency(keys[0], keys[1]);
		dagm.addDependency(keys[1], keys[2]);
		dagm.addDependency(keys[3], keys[0]);
		dagm.addDependency(keys[5], keys[2]);
		dagm.addDependency(keys[4], keys[3]);
		dagm.addDependency(keys[5], keys[4]);
		dagm.addDependency(keys[4], keys[7]);
		dagm.addDependency(keys[5], keys[8]);
		dagm.addDependency(keys[7], keys[6]);
		dagm.addDependency(keys[8], keys[7]);
		dagm.addDependency(keys[9], keys[6]);
		dagm.addDependency(keys[7], keys[10]);
		dagm.addDependency(keys[11], keys[8]);
		dagm.addDependency(keys[10], keys[9]);
	}

	@Before
	public void setUpHard() {
		hard = new DAGMap<Character,String>();
		for(int i = 0; i < 15; i++) {
			hard.put(keys[i], null);
		}
		
		 /* A better dag to test max flow.
		 * Only one possible way of getting flow of 3.
		 * 
		 * hard
		 * 0 => 3 => 6 =>[9]<= 12
		 * ^    ^    |    ^    ^
		 * U    |    v    U    U
		 * 1 -> 4 => 7 => 10-> 13
		 * ^    ^    |    ^    ^
		 * U    U    v    |    U
		 * 2 <=[5]-> 8 => 11=> 14
		 */
		hard.addDependency(keys[1], keys[0]);
		hard.addDependency(keys[2], keys[1]);
		hard.addDependency(keys[0], keys[3]);
		hard.addDependency(keys[1], keys[4]);
		hard.addDependency(keys[5], keys[2]);
		hard.addDependency(keys[4], keys[3]);
		hard.addDependency(keys[5], keys[4]);
		hard.addDependency(keys[3], keys[6]);
		hard.addDependency(keys[4], keys[7]);
		hard.addDependency(keys[5], keys[8]);
		hard.addDependency(keys[6], keys[7]);
		hard.addDependency(keys[7], keys[8]);
		hard.addDependency(keys[6], keys[9]);
		hard.addDependency(keys[7], keys[10]);
		hard.addDependency(keys[8], keys[11]);
		hard.addDependency(keys[10], keys[9]);
		hard.addDependency(keys[11], keys[10]);
		hard.addDependency(keys[12], keys[9]);
		hard.addDependency(keys[10], keys[13]);
		hard.addDependency(keys[11], keys[14]);
		hard.addDependency(keys[13], keys[12]);
		hard.addDependency(keys[14], keys[13]);
	}
	
	@Before
	public void setUpHarder() {
		harder = new DAGMap<Character,String>();
		for(int i = 0; i < 21; i++) {
			harder.put(keys[i], null);
		}
		harder.addDependency(keys[1], keys[0]);
		harder.addDependency(keys[1], keys[4]);
		harder.addDependency(keys[1], keys[2]);
		harder.addDependency(keys[0], keys[3]);
		harder.addDependency(keys[3], keys[5]);
		harder.addDependency(keys[5], keys[7]);
		harder.addDependency(keys[7], keys[9]);
		harder.addDependency(keys[4], keys[6]);
		harder.addDependency(keys[6], keys[8]);
		harder.addDependency(keys[8], keys[10]);
		harder.addDependency(keys[2], keys[11]);
		harder.addDependency(keys[11], keys[10]);
		harder.addDependency(keys[10], keys[9]);
		harder.addDependency(keys[9], keys[18]);
		harder.addDependency(keys[18], keys[19]);
		harder.addDependency(keys[10], keys[12]);
		harder.addDependency(keys[12], keys[14]);
		harder.addDependency(keys[14], keys[16]);
		harder.addDependency(keys[16], keys[19]);
		harder.addDependency(keys[11], keys[13]);
		harder.addDependency(keys[13], keys[15]);
		harder.addDependency(keys[15], keys[17]);
		harder.addDependency(keys[17], keys[20]);
		harder.addDependency(keys[20], keys[19]);
		
	}
	
	@Test
	public void testPutGetContainsRemoveWithNull() {
		Character key1 = new Character('m');
		Character key2 = new Character('n');
		String value1 = "String";
		String value2 = null;
		dagm.put(key1, value1);
		assertEquals(true, dagm.containsKey(key1));
		assertEquals(true, dagm.containsValue(value1));
		assertEquals(true, dagm.get(key1) == value1);
		dagm.put(key2, value2);
		try {
			dagm.put(key1, value1);
			fail("Putting in already existent key should throw IllegalArgumentException");
		}
		catch (IllegalArgumentException e) {}
		try {
			dagm.put(null, value1);
			fail("Putting in invalid key should throw IllegalArgumentException");
		}
		catch (IllegalArgumentException e) {}
		assertEquals(true, dagm.containsKey(key2));
		assertEquals(true, dagm.containsValue(value2));
		assertEquals(true, dagm.get(key2) == value2);
		dagm.remove(key1);
		try {
			dagm.remove(key1);
			fail("Removing non-existent vertex should throw IllegalArgumentException");
		}
		catch (IllegalArgumentException e) {}
		try {
			dagm.remove(null);
			fail("Removing invalid key null should throw IllegalArgumentException");
		}
		catch (IllegalArgumentException e) {}
		assertEquals(false, dagm.containsKey(key1));
	}
	
	@Test
	public void testCreateCycle() {
		try {
			dagm.addDependency(keys[1], keys[4]);
			fail("Meant to thow error when creating a cycle.");
		}
		catch (IllegalArgumentException e) {}
	}
	
	@Test
	public void testGetDependents() {
		Set<Character> dep = dagm.getDependents(keys[0]);
		assertEquals(2, dep.size());
		assertEquals(true, dep.contains(keys[1]));
		assertEquals(true, dep.contains(keys[2]));
		dep = dagm.getDependents(keys[11]);
		assertEquals(5, dep.size());
		assertEquals(true, dep.contains(keys[6]));
		assertEquals(true, dep.contains(keys[7]));
		assertEquals(true, dep.contains(keys[8]));
		assertEquals(true, dep.contains(keys[9]));
		assertEquals(true, dep.contains(keys[10]));
	}
	
	@Test
	public void testIterator() {
		System.out.println("Iterator: dagm");
		Iterator<Character> it = dagm.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + ", ");
		}
		System.out.println();
		System.out.println("Iterator: harder");
		it = harder.iterator();
		while(it.hasNext()) {
			System.out.print(it.next() + ", ");
		}
	}
	
	@Test
	public void testMaxFlow() {
		System.out.println("Max Flow: harder");
		assertEquals(3, harder.getMaxFlow(keys[1], keys[19]));
		System.out.println("Max Flow: dagm f->g");
		assertEquals(1, dagm.getMaxFlow(keys[5], keys[6]));
		System.out.println("Max Flow: dagm f->c");
		//assertEquals(2, dagm.getMaxFlow(keys[5], keys[2]));
		System.out.println("Max Flow: hard f->j");
		assertEquals(3, hard.getMaxFlow(keys[5], keys[9]));
	}
	
	@Test
	public void testGetWidth() {
		assertEquals(2, dagm.getWidth());
		assertEquals(3, hard.getWidth());
	}
	
	@Test
	public void testCloneEquals() {
		DAGMap<Character,String> dagmclone = (DAGMap<Character,String>) dagm.clone();
		
		assertEquals(true, dagm.equals(dagmclone));
		assertEquals(true, dagmclone.equals(dagm));
		assertEquals(false, dagm == dagmclone);
		
		dagmclone.put('z', "Hello");
		assertEquals(false, dagm.equals(dagmclone));
		dagm.put('z', "Hello");
		
	}
	
	@Test public void testRequirements() {
		Set<Character> requirements = dagm.getRequirements(keys[6]);
		assertEquals(7, requirements.size());
		assertEquals(true, requirements.contains(keys[4]));
		assertEquals(true, requirements.contains(keys[5]));
		assertEquals(true, requirements.contains(keys[7]));
		assertEquals(true, requirements.contains(keys[8]));
		assertEquals(true, requirements.contains(keys[9]));
		assertEquals(true, requirements.contains(keys[10]));
		assertEquals(true, requirements.contains(keys[11]));
	}
	
	@Test
	public void testToString() {
		System.out.println("ToString:");
		System.out.println(dagm.toString());
	}
	
	@Test
	public  void testLongestPath() {
		assertEquals(5,dagm.getLongestPath());
	}
}
