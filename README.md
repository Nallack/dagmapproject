﻿#Tasks to be done

1. Implement All Methods
2. Create JUnit Tests
3. Write Report

#Eclipse Setup

After getting access to the repo page at
https://bitbucket.org/Nallack/dagmapproject

1. In eclipse goto File->Import
2. Select Git->Projects from Git
3. Select Clone URI
4. On the website at [here](https://bitbucket.org/Nallack/dagmapproject)
on the right there should be repo link, copy
the HTTPS URI.
5. Paste the URI into the URI field in Eclipse. 
It should automagically fill out the other fields.
 Dont bother filling in port.
6. Type your bitbucket password and Secure Store it
if you want ecplipse to remember it.